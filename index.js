const express = require('express');
const keys = require('./config/keys');
var stripe = require('stripe')('sk_test_UYyolFCt3zbr75hUJOv0zVkB');
const exphbs = require('express-handlebars');
var bodyParser = require('body-parser')
const app = express();
const cors = require('cors');

// app.use(allowCORS)
// app.use(cors());var cors = require ('cors')

// var allowCORS = function(req,res,next){
//   res.append('Access-Control-Allow-Origin', ['*']);
//   res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
//   res.append('Access-Control-Allow-Headers', 'Content-Type');
//   next();
// }


// Handlebars Middleware
app.engine('handlebars',exphbs({defaultLayout:'main'}));
app.set('view engine', 'handlebars');

// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// Set Static Folder
app.use(express.static(`${__dirname}/public`));



// Index Route
app.get('/', (req, res) => {
  res.render('index', {
    stripePublishableKey: 'pk_test_50JRjLtzVIM5wRrmnOnnfCyL'
  });
});


// Charge Route
app.post('/charge', (req, res) => {
  console.log(req.body)
  
  stripe.customers.create({
    email: req.body.stripeEmail,
    source: req.body.stripeToken
  })
  .then(customer => stripe.charges.create({
    amount:req.body.amount,
    description:req.body.description,
    currency: req.body.currency,
    customer: customer.id
  }))
  .then(charge => res.end('success'));
});

const port = process.env.PORT || 5000;
app.listen(port, () => {
    console.log(`Server started on port ${5000}`);
  });


  